//
//  AppDelegate.h
//  EndOfAyah
//
//  Created by Mani Muridi on 7/10/14.
//  Copyright (c) 2014 manimuridi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
