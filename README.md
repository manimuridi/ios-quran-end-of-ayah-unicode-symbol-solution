# IOS Quran End of Ayah Unicode Symbol Solution #

Simple IOS Project contains end of ayah symbols used in the quran.

### How do I get set up? ###

Make sure you add your fonts in the plist projectname-Info.plist
in the "Fonts provided by application" array property.

and then...

```
#!objective-c

    /** 1. Find Real Font Name
     * This loop lists all installed/supported fonts in the app
     * this is to find the real names of the fonts
     * since this can be different than the .ttf filename
     *
     **/
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
    
    /** 2. Find font name in output logs from the loop.
     * Installed Arabic Fonts
     * Scheherazade
     * me_quran
     * ArabeyesQr
     * _PDMS_Saleem_QuranFont
     *
     **/
    
    /* 3. Use Arabic Ornate Left/Right Paranthesis with the number in between
     *
     * Symbol: ﴾
     * Unicode: U+FD3E
     * C-Language Unicode: \uFD3E
     *
     * Number: ٢٨٦
     *
     * Symbol: ﴿
     * Unicode: U+FD3F
     * C-Language Unicode: \uFD3F
     *
     * Combined  '﴾' + '٢٨٦' + '﴿' = '﴾٢٨٦﴿' -> unicode -> \uFD3E٢٨٦\uFD3F
     */
    
    NSString *str = @"\uFD3E١\uFD3F";
    self.theLabel.font = [UIFont fontWithName:@"me_quran" size:20];
    self.theLabel.text = str;
    
    str = @"\uFD3E١٥\uFD3F";
    self.theLabel2.font = [UIFont fontWithName:@"me_quran" size:20];
    self.theLabel2.text = str;
    
    str = @"\uFD3E٢٨٦\uFD3F";
    self.theLabel3.font = [UIFont fontWithName:@"me_quran" size:20];
    self.theLabel3.text = str;
```

### Resources ###
Arabic Unicode List - http://en.wikipedia.org/wiki/Arabic_script_in_Unicode

Fonts (me_quran font) - http://tanzil.net/wiki/Quranic_Fonts